#include <iostream>
#include "hello.h"

void hello(){
    #ifdef NDEBUG
    std::cout << "Hello World from Package Release!" <<std::endl;
    #else
    std::cout << "Hello World from Package Debug!" <<std::endl;
    #endif
}
